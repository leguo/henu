% !Mode:: "TeX:UTF-8"
%% 请使用 XeLaTeX 编译本文.
\documentclass[a4paper,amsmath,amssymb]{article}  %%%revtex4
\usepackage{fontspec}
%%%\setmainfont[Mapping=tex-text]{KaiTi}
\usepackage{ctex}
\usepackage{caption}
\captionsetup[figure]{labelfont={bf},labelformat={default},labelsep=period,name={Figure}}

\usepackage{geometry}
\geometry{left=2cm,right=2cm,top=1cm,bottom=2cm}
\usepackage{hyperref}

\usepackage{fancyhdr} % 添加页眉页脚
\lhead{}
\chead{}
\rhead{\thepage}
\lfoot{}
\cfoot{}
\rfoot{}
\pagestyle{plain} % set pagestyle for the document
\renewcommand{\headrulewidth}{0pt} %改为0pt即可去掉页眉下面的横线
\renewcommand{\footrulewidth}{0pt} %改为0pt即可去掉页脚上面的横线 0.4pt
%%\includepdfset{pagecommand={\thispagestyle{fancy}}} % pdfpages宏包插入文件无页码的解决

%%\fontsize{10}{1.5}    %%{字号}{基本行距}
\linespread{1.2} %%对于全局设置应该把行距设置放在导言区，对于临时设置应该在行距命令后面加 \selectfont。


\usepackage{enumerate}
\usepackage{graphicx}     %Allows \includegraphics{figure.pdf}
\usepackage[T1]{fontenc}
\usepackage{times}         %Use Times New Roman font
\usepackage{mathrsfs} %Allows \mathscr{HELLO}
\usepackage{myterms}

\usepackage{indentfirst} %%首行缩进
%%% new added %%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{color,fancybox,epsf,rotating,colordvi}
\def\red#1{\textcolor{red}{#1}}
\def\cyan#1{\textcolor{cyan}{#1}}
\def\blue#1{\textcolor{blue}{#1}}
\def\magen#1{\textcolor{magenta}{#1}}
\def\green#1{\textcolor{green}{#1}}
\def\brown#1{\textcolor{brown}{#1}}
\def\black#1{\textcolor{black}{#1}}
\def\yellow#1{\textcolor{yellow}{#1}}
\def\darkg#1{\textcolor{darkgray}{#1}}
\def\gray#1{\textcolor{gray}{#1}}
\def\lightg#1{\textcolor{lightgray}{#1}}
\def\orange#1{\textcolor{orange}{#1}}
\def\violet#1{\textcolor{violet}{#1}}
\def\purple#1{\textcolor{purple}{#1}}
\def\white#1{\textcolor{white}{#1}}

\def\bbf#1{{\bf \textcolor{blue}{#1}}}
\def\rbf#1{{\bf \textcolor{red}{#1}}}
\def\mbf#1{{\bf \textcolor{magenta}{#1}}}

\newcommand{\paper}{\textcolor{red}{$\heartsuit$ Paper~}}
\newcommand{\SLASH}[2]{\makebox[#2ex][l]{$#1$}/}
\newcommand{\pslash}{\SLASH{p}{.2}}
%%  25 一号 /Huge
%%　　20 二号 /huge
%%　　17 三号 /LARGE
%%　　14 四号 /Large
%%　　12 小四号 /large
%%　　10 五号 /normalsize
%%　　9 小五号 /small
%%　　8 六号 /footnotesize
%%　　7 小六号 /scriptsize
%%　　5 七号 /tiny
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}

\large
%%%\thispagestyle{empty} % this page does not have a header

\title{\LARGE \bf 2023年9月研究进展
}
\author{%%%\emph{Paper is a lifestyle, we love paper, we love expressing. Let the world hear our voice!}\\
\Large \bf 郭\ 蕾
%\vspace{0.0cm}
%{zhujy@henu.edu.cn}%%\normalsize
%\vspace{0.0cm} \\
%{\it Henan University}
}
%\date{\emph{2021-09-30}}
\maketitle\thispagestyle{plain}%fancy

\vspace{-1cm}

\section{物理分析}

\subsection{紧凑空间零轻子末态的超对称粒子寻找研究}
9月份，组内成员研究了新的信号点网格，并于29号在EWK会议上申请信号点，进展顺利。
接下来我要根据新的信号点检查一下信号的截面和截面误差与之前的结果是否一致。

新的信号点的申请是考虑和CMS合作组检查之后，我们修改了一些产生样本中的条件，预计可以增加信号的样本数，有助于信号显著性的提升。
相关细节在Jira中\href{https://its.cern.ch/jira/browse/ATLMCPROD-10794/}{ Signal MC request}.
The features of the new grid relative to previous grids are:

mjj>0 instead of 500 GeV

deltaeta>2.5 instead of 3.0

ptj>30 instead of 20 GeV

bwcutoff=15 instead of 10000

including susystrong diagrams instead of excluding

\subsection{紧凑空间软轻子末态的超对称粒子寻找研究}
无进展

\subsection{H->aa->yyττ lep-had Analysis}
本月完成了该分析的hadhad分析道的文献阅读，并熟悉该分析道的研究流程和内容。hadhad分析道已经得到初步结果，给出了截面上限。对于刚起步的lephad分析道，我们计划申请样本。

本月我完成了信号点的DAOD的研究，计算了DAOD/AOD的效率
\begin{figure}[h]
    \centering
    \includegraphics[width=16cm]{My_Report/higgs.png}
    \caption{meeting}
    \label{fig:higgs meeting}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=16cm]{My_Report/efficiency.png}
    \caption{signal DAOD efficiency}
    \label{fig:efficiency}
\end{figure}

\clearpage
\section{硬件项目}

1）确定了QT，开始时间是2023年6月15日，预计明年6月结束。9月21日在中英itk组的WP12会议上代表IHEP站点给了一个QT进展的报告。\href{https://indico.cern.ch/event/1327377/contributions/5585996/attachments/2716023/4722818/IHEP_21092023_WP12_QT_update_Lei.pdf/}{ QT update}.
\begin{figure}[h]
    \centering
    \includegraphics[width=16cm]{My_Report/QT.png}
    \caption{QT}
    \label{fig:QT}
\end{figure}

2）ATLAS-ITk硅微条径迹探测器项目。
校准胶水重量问题，8月我们接收到UK寄送的新胶水（之前用的胶水都是过期的）校准胶重，改变自动点胶机的点胶程序，可以有效的控制胶重。9月，我们还进行了多次测量，可以得到胶重的稳定性较好。

九月，提交了PPB2 Hybrid和Module的申请，并回复了相关的问题和建议，目前正在着力解决冷热循环的测试，完成TC（thermal cycling）可以通过Module的申请。


% 这里可以推荐一个好玩的教程：\href{https://streamlit.io/}{Streamlit}.可以进行简单的app开发。

%\clearpage
\section{下月计划}

\begin{itemize}\itemsep 6pt
	\item SUSY物理分析：检查信号点的截面和截面误差
	\item HDBS物理分析：和中山大学刘洋老师一起加入该分析，目前完成了信号点的DAOD样本生产，下一步进行本底和数据的DAOD样本产生，并搭建产生Ntuple的框架。
	\item 硬件项目：确定QT后根据QT计划着手相关生产工作，计划十月开展生产工具的SQ工作，将所有工具整理好，便于直接在生产中使用。
\end{itemize}
   






% \subsection{阅读了文献Ref.\cite{Crivellin:2021ubm}}
% Ref.\cite{Crivellin:2021ubm}主要讲了......

% 特殊符号输入示例: $\alpha$, $\pm$, $\mp$, $\gg$, $\ll$, $\equiv$, $\approx$, $\qquad\qquad$  $\sin60^{\circ}=\sqrt{3}/2$

% 公式输入示例：
% \begin{eqnarray}
% 	\Omega_\chi h^2
% 	&\equiv& m_\chi n_0 \frac{h^2}{\rho_c}
% 	\nonumber\\
% 	&=&\left(\frac{m_{\chi}/T_{\rm f}}{10}\right)\sqrt{\frac{g_*}{100}} ~ \frac{0.847 \!\times\! 10^{-27}{\rm cm}^3{\rm s}^{-1}}{\langle v \sigma_{\rm eff}\rangle}
% \end{eqnarray}

% 参考文献可修改文件ref.bib添加。LaTeX更多问题解决办法可百度搜索，以及从arXiv网站下载论文源文件。

% \subsection{重复出了Ref.\cite{Fowlie:2021ldv}的计算方法}

% \subsection{课题XXX完成了哪一步}


% \subsection{学会了XXX技术}
% \begin{itemize}\itemsep 3pt
% 	\item 工作环境: Linux操作系统（例如Ubuntu-22.04-desktop-amd64）

% 	\item 常用语言: Python、C++、Fortran、Mathematica

% 	\item 高阶技术: 机器学习、深度学习

% 	\item 模型构建及唯象学研究\\
% 	SARAH: \url{https://sarah.hepforge.org/} \\
% 	FlexibleSUSY:  \url{https://flexiblesusy.hepforge.org/}
	
% 	\item Higgs数据限制: HiggsBounds、HiggsSignals
	
% 	\item 暗物质: MicrOMEGAs
	
% 	\item 超对称实验数据限制: SModelS、CheckMATE
	
% 	\item 对撞机信号模拟: MadGraph, ......
	
% 	\item 模型NMSSM: NMSSMTools
	
% 	\item 模型MSSM: EasyScan\_HEP、susy-hit、FeyHiggs、SuSpect、SPheno、SusHig、SOFTSUSY
	
% 	\item 模型SM+Scalars: ScanerS
	
% 	\item 模型2HDM: 2HDMC
	
% 	\item 模型N2HDM: N2HDECAY
	
% 	\item 文档编辑: LaTeX
	
% 	\item 文献检索: 
% 	\url{https://arxiv.org/} $\quad$
% 	\url{https://inspirehep.net/}
		
% \end{itemize}



% \section{遇到什么问题？如何解决的？}

% \begin{itemize}\itemsep 6pt
% 	\item 问题1：......\\ 
% 	如何解决的......
% 	\item 问题2：......\\
% 	如何解决的......
% 	\item 问题3：......\\ 
% 	如何解决的......	
% \end{itemize}


% \section{计算结果展示和分析}

% \subsection{重复Ref.\cite{Fowlie:2021ldv}的结果}
% \begin{figure}[h]
% 	\centering
% 	\includegraphics[width=12cm]{fig1.png}
% 	\vspace*{-0.3cm}
% 	\caption{。。。。。。。。。。。。。。。。。。。。。} 
% 	\label{fig1}
% \end{figure}

% 	重复出了论文\cite{Fowlie:2021ldv}中的图，如Fig.\ref{fig1}所示。图的物理意义是。。。。。。

% \subsection{利用XXX技术得到的结果}


% \subsection{课题XXX最近的计算结果}





% %%%\section*{Key references}
% \bibliographystyle{JHEP}
% \bibliography{ref}
% % find eprint 2007.09252 or 2109.02650 or 2109.13426 or 1908.08215 or 1806.05264 or 2109.14030 or 1806.09478
\end{document}
